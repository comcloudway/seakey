use enigo::*;

pub struct KeyMap {
    // MOD KEYS
    pub mod0: Key,
    pub mod1: Key,
    pub mod2: Key,

    // SPECIAL
    pub spec0: Key,

    // ALPHABETICAL
    pub alp0: Key,
    pub alp1: Key,
    pub alp2: Key,
    pub alp3: Key,
    pub alp4: Key,
    pub alp5: Key,
    pub alp6: Key,
    pub alp7: Key,
    pub alp8: Key,
    pub alp9: Key,

    // NUMBER / SYMBOLS
    pub syn0: Key,
    pub syn1: Key,
    pub syn2: Key,
    pub syn3: Key,
    pub syn4: Key,
    pub syn5: Key,
    pub syn6: Key,
    pub syn7: Key,
}
impl KeyMap {
    pub fn list(&self) -> Vec<Key> {
        return vec![
            self.mod0, self.mod1, self.mod2,
            self.spec0,
            self.alp0, self.alp1, self.alp2, self.alp3, self.alp4, self.alp5, self.alp6, self.alp7, self.alp8, self.alp9,
            self.syn0, self.syn1, self.syn2, self.syn3, self.syn4, self.syn5, self.syn6, self.syn7,
        ];
    }
}

pub const KEYMAP0: KeyMap = KeyMap {
    mod0: Key::Control,
    mod1: Key::Meta,
    mod2: Key::Shift,

    spec0: Key::Return,

    alp0: Key::Layout('q'),
    alp1: Key::Layout('w'),
    alp2: Key::Layout('e'),
    alp3: Key::Layout('r'),
    alp4: Key::Layout('t'),
    alp5: Key::Layout('y'),
    alp6: Key::Layout('u'),
    alp7: Key::Layout('i'),
    alp8: Key::Backspace,
    alp9: Key::Space,

    syn0: Key::Layout('1'),
    syn1: Key::Layout('2'),
    syn2: Key::Layout('3'),
    syn3: Key::Layout('4'),
    syn4: Key::Layout('5'),
    syn5: Key::Layout('6'),
    syn6: Key::Layout('7'),
    syn7: Key::Layout('8'),
};
pub const KEYMAP1: KeyMap = KeyMap {
    mod0: Key::Control,
    mod1: Key::Meta,
    mod2: Key::Shift,

    spec0: Key::Return,

    alp0: Key::Layout('o'),
    alp1: Key::Layout('p'),
    alp2: Key::Layout('a'),
    alp3: Key::Layout('s'),
    alp4: Key::Layout('d'),
    alp5: Key::Layout('f'),
    alp6: Key::Layout('g'),
    alp7: Key::Layout('h'),
    alp8: Key::Backspace,
    alp9: Key::Space,

    syn0: Key::Layout('1'),
    syn1: Key::Layout('2'),
    syn2: Key::Layout('3'),
    syn3: Key::Layout('4'),
    syn4: Key::Layout('5'),
    syn5: Key::Layout('6'),
    syn6: Key::Layout('7'),
    syn7: Key::Layout('8'),
};
pub const KEYMAP2: KeyMap = KeyMap {
     mod0: Key::Control,
    mod1: Key::Meta,
    mod2: Key::Shift,

    spec0: Key::Return,

    alp0: Key::Layout('j'),
    alp1: Key::Layout('k'),
    alp2: Key::Layout('l'),
    alp3: Key::Layout('y'),
    alp4: Key::Layout('x'),
    alp5: Key::Layout('c'),
    alp6: Key::Layout('v'),
    alp7: Key::Layout('b'),
    alp8: Key::Backspace,
    alp9: Key::Space,

    syn0: Key::Layout('1'),
    syn1: Key::Layout('2'),
    syn2: Key::Layout('3'),
    syn3: Key::Layout('4'),
    syn4: Key::Layout('5'),
    syn5: Key::Layout('6'),
    syn6: Key::Layout('7'),
    syn7: Key::Layout('8'),
};
pub const KEYMAP3: KeyMap = KeyMap {
     mod0: Key::Control,
    mod1: Key::Meta,
    mod2: Key::Shift,

    spec0: Key::Return,

    alp0: Key::Layout('n'),
    alp1: Key::Layout('m'),
    alp2: Key::LeftArrow,
    alp3: Key::RightArrow,
    alp4: Key::Layout('ß'),
    alp5: Key::Layout(','), // UNUSED
    alp6: Key::Layout('.'), // UNUSED
    alp7: Key::Layout('+'),
    alp8: Key::Backspace,
    alp9: Key::Space,

    syn0: Key::Layout('<'),
    syn1: Key::Layout(','),
    syn2: Key::Layout('7'),
    syn3: Key::Layout('8'),
    syn4: Key::Layout('9'),
    syn5: Key::Layout('0'),
    syn6: Key::Layout('.'),
    syn7: Key::Layout('-'),
};
