extern crate midir;

mod banks;
use notify_rust::Notification;
use crate::banks::{KEYMAP0, KEYMAP1, KEYMAP2, KEYMAP3, KeyMap};
use enigo::*;
use midir::{Ignore, MidiInput};
use std::error::Error;
use std::io::{stdin, stdout, Write};

fn main() {
    match run() {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err),
    }
}

fn run() -> Result<(), Box<dyn Error>> {
    let mut enigo = Enigo::new();

    // KEYMAP BANKS
    // BANKS
    let mut matrix: [usize; 2] = [0, 0];
    let banks: [[KeyMap; 2]; 2] = [[KEYMAP0, KEYMAP1], [KEYMAP2, KEYMAP3]];

    let mut input = String::new();
    let mut midi_in = MidiInput::new("midir reading input")?;
    midi_in.ignore(Ignore::None);

    // Get an input port (read from console if multiple are available)
    let in_ports = midi_in.ports();
    let in_port = match in_ports.len() {
        0 => return Err("no input port found".into()),
        1 => {
            println!(
                "Choosing the only available input port: {}",
                midi_in.port_name(&in_ports[0]).unwrap()
            );
            &in_ports[0]
        }
        _ => {
            println!("\nAvailable input ports:");
            for (i, p) in in_ports.iter().enumerate() {
                println!("{}: {}", i, midi_in.port_name(p).unwrap());
            }
            print!("Please select input port: ");
            stdout().flush()?;
            let mut input = String::new();
            stdin().read_line(&mut input)?;
            in_ports
                .get(input.trim().parse::<usize>()?)
                .ok_or("invalid input port selected")?
        }
    };

    println!("\nOpening connection");
    let in_port_name = midi_in.port_name(in_port)?;

    fn reset_keys(enigo: &mut enigo::Enigo, keymap: &KeyMap) {
        for key in keymap.list() {
            enigo.key_up(key);
        }
    }
    fn reset_all(enigo: &mut enigo::Enigo, banks: &[[KeyMap; 2];2]) {
        for c in banks {
            for r in c {
                reset_keys(enigo, r);
            }
        }
    }

    // _conn_in needs to be a named parameter, because it needs to be kept alive until the end of the scope
    let _conn_in = midi_in.connect(
        in_port,
        "midir-read-input",
        move |_stamp, message, _| {
            if message[0] >= 128 && message[0] <= 143 {
                // NOTE OFF
                match message[1] % 24 {
                    // BANK SELECTION
                    12 => {
                        matrix[0] = 0;
                        reset_all(&mut enigo, &banks);
                    },
                    14 => {
                        matrix[1] = 0;
                        reset_all(&mut enigo, &banks);
                    },
                    // MODIFIER KEYS
                    13 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].mod0);
                    }
                    15 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].mod1);
                    }
                    16 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].mod2);
                    }
                    // SPECIAL
                    11 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].spec0);
                    }
                    // ALPHABETICAL
                    17 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp0);
                    }
                    19 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp1);
                    }
                    21 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp2);
                    }
                    23 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp3);
                    }
                    0 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp4);
                    }
                    2 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp5);
                    }
                    4 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp6);
                    }
                    5 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp7);
                    }
                    7 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp8);
                    }
                    9 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].alp9);
                    }

                    // SYMBOL / NUMBER
                    18 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn0);
                    }
                    20 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn1);
                    }
                    22 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn2);
                    }
                    1 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn3);
                    }
                    3 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn4);
                    }
                    6 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn5);
                    }
                    8 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn6);
                    }
                    10 => {
                        enigo.key_up(banks[matrix[0]][matrix[1]].syn7);
                    }
                    _ => (),
                };
            } else if message[0] >= 140 && message[0] <= 159 {
                // NOTE ON
                match message[1] % 24 {
                    // BANK SELECTION
                    12 => {
                        matrix[0] = 1;
                        reset_all(&mut enigo, &banks);
                    },
                    14 => {
                        matrix[1] = 1;
                        reset_all(&mut enigo, &banks);
                    },
                    // MODIFIER KEYS
                    13 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].mod0);
                    }
                    15 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].mod1);
                    }
                    16 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].mod2);
                    }
                    // SPECIAL
                    11 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].spec0);
                    }
                    // ALPHABETICAL
                    17 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp0);
                    }
                    19 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp1);
                    }
                    21 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp2);
                    }
                    23 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp3);
                    }
                    0 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp4);
                    }
                    2 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp5);
                    }
                    4 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp6);
                    }
                    5 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp7);
                    }
                    7 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp8);
                    }
                    9 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].alp9);
                    }

                    // SYMBOL / NUMBER
                    18 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn0);
                    }
                    20 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn1);
                    }
                    22 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn2);
                    }
                    1 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn3);
                    }
                    3 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn4);
                    }
                    6 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn5);
                    }
                    8 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn6);
                    }
                    10 => {
                        enigo.key_down(banks[matrix[0]][matrix[1]].syn7);
                    }
                    _ => (),
                };
            } else if message[0] >= 160 && message[0] <= 175 {
                // POLYPHONIC AFTERTOUCH - pressure [never detected with seboard block]
            } else if message[0] >= 176 && message[0] <= 191 {
                //CONTROL/MODE CHANGE
            } else if message[0] >= 192 && message[0] <= 207 {
                // PROGRAM CHANGE
            } else if message[0] >= 208 && message[0] <= 223 {
                // CHANNEL AFTERTOUCH - pressure
            } else if message[0] >= 224 && message[0] <= 239 {
                // PITCH WHEEL CONTROL
            }
        },
        (),
    )?;

    println!(
        "Connection open, reading input from '{}' (press enter to exit) ...",
        in_port_name
    );

    input.clear();
    stdin().read_line(&mut input)?; // wait for next enter key press

    println!("Closing connection");
    Ok(())
}
